// Array para almacenar los contactos
let contactos = [];

// Constructor de objetos Contacto
function Contacto(nombre, apellido, telefono, correo) {
  this.nombre = nombre;
  this.apellido = apellido;
  this.telefono = telefono;
  this.correo = correo;
}

// Función para agregar un nuevo contacto
function agregarContacto() {
  const nombre = document.getElementById('nombre').value;
  const apellido = document.getElementById('apellido').value;
  const telefono = document.getElementById('telefono').value;
  const correo = document.getElementById('correo').value;

  if (nombre && apellido) {
    const nuevoContacto = new Contacto(nombre, apellido, telefono, correo);
    contactos.push(nuevoContacto);
    mostrarContactos();
    actualizarTotalContactos();
    limpiarFormulario();
  } else {
    alert('Por favor, ingrese el nombre y apellido del contacto.');
  }
}

// Función para mostrar los contactos en la lista
function mostrarContactos() {
  const listaContactos = document.getElementById('listaContactos');
  listaContactos.innerHTML = ''; // Limpiar la lista antes de mostrar

  contactos.forEach((contacto, index) => {
    const li = document.createElement('li');
    li.className = 'list-group-item d-flex justify-content-between align-items-center';

    const contactInfo = document.createElement('div');
    contactInfo.className = 'contact-info';
    contactInfo.innerHTML = `
      <strong>${contacto.nombre} ${contacto.apellido}</strong><br>
      <span>Teléfono: ${contacto.telefono}</span><br>
      <span>Correo: ${contacto.correo}</span>
    `;

    const botonEliminar = document.createElement('button');
    botonEliminar.className = 'btn btn-danger btn-sm';
    botonEliminar.textContent = 'Eliminar';
    botonEliminar.onclick = () => eliminarContacto(index);

    li.appendChild(contactInfo);
    li.appendChild(botonEliminar);
    listaContactos.appendChild(li);
  });
}

// Función para eliminar un contacto por índice
function eliminarContacto(index) {
  contactos.splice(index, 1);
  mostrarContactos();
  actualizarTotalContactos();
}

// Función para actualizar el indicador de total de contactos
function actualizarTotalContactos() {
  const totalContactos = document.getElementById('totalContactos');
  totalContactos.textContent = `Total de contactos: ${contactos.length}`;
}

// Función para buscar contactos por nombre
function buscarContacto() {
  const textoBusqueda = document.getElementById('buscarContacto').value.toLowerCase();
  const contactosFiltrados = contactos.filter(contacto => {
    const nombreCompleto = contacto.nombre.toLowerCase() + ' ' + contacto.apellido.toLowerCase();
    return nombreCompleto.includes(textoBusqueda);
  });

  mostrarContactosFiltrados(contactosFiltrados);
}

// Función para mostrar una lista de contactos filtrados
function mostrarContactosFiltrados(contactosFiltrados) {
  const listaContactos = document.getElementById('listaContactos');
  listaContactos.innerHTML = ''; // Limpiar la lista antes de mostrar

  contactosFiltrados.forEach((contacto, index) => {
    const li = document.createElement('li');
    li.className = 'list-group-item d-flex justify-content-between align-items-center';

    const contactInfo = document.createElement('div');
    contactInfo.className = 'contact-info';
    contactInfo.innerHTML = `
      <strong>${contacto.nombre} ${contacto.apellido}</strong><br>
      <span>Teléfono: ${contacto.telefono}</span><br>
      <span>Correo: ${contacto.correo}</span>
    `;

    const botonEliminar = document.createElement('button');
    botonEliminar.className = 'btn btn-danger btn-sm';
    botonEliminar.textContent = 'Eliminar';
    botonEliminar.onclick = () => eliminarContacto(index);

    li.appendChild(contactInfo);
    li.appendChild(botonEliminar);
    listaContactos.appendChild(li);
  });
}

// Función para limpiar el formulario
function limpiarFormulario() {
  document.getElementById('nombre').value = '';
  document.getElementById('apellido').value = '';
  document.getElementById('telefono').value = '';
  document.getElementById('correo').value = '';
}

// Event listeners para el formulario y el campo de búsqueda
document.getElementById('formularioContacto').addEventListener('submit', (event) => {
  event.preventDefault(); // Evitar el envío del formulario por defecto
  agregarContacto();
});

document.getElementById('buscarContacto').addEventListener('input', buscarContacto);

// Eliminar todos los contactos
document.getElementById('eliminarTodos').addEventListener('click', () => {
  if (confirm('¿Está seguro de eliminar todos los contactos?')) {
    contactos = [];
    mostrarContactos();
    actualizarTotalContactos();
  }
});

// Mostrar contactos al iniciar la aplicación
mostrarContactos();
actualizarTotalContactos();

